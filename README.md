<h2>Author: Guilherme de Souza Topanotti</h2>

<h3 dir="auto">Janeiro de 2020</h3>

Descrição:
<p dir="auto"><span style="font-size:14px">Exemplo de aplicação utilizando apis REST com JWT&nbsp;e H2DB.</span></p>

<p dir="auto">&nbsp;</p>

<h2 dir="auto">Documenta&ccedil;&atilde;o da api:</h2>

<p dir="auto"><span style="font-size:14px">http://localhost:8080/api/swagger-ui.html</span></p>

<h2 dir="auto">Para executar:</h2>
mvn clean install&nbsp;

java -jar  /target/authservice-0.0.1-SNAPSHOT.jar


<h2 dir="auto">Documentação das apis</h2>
localhost:8080/api/swagger-ui

<h2 dir="auto">Orientações</h2>

Para se obter o token de autenticação, deve bater na api localhost:8080/api/login, com o método POST&nbsp;
e passando o objeto abaixo:&nbsp;
{
	"login":"guilherme",
	"senha":"123"
}


O token tem duração de 2 horas.