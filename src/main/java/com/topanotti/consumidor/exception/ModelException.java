package com.topanotti.consumidor.exception;

public class ModelException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ModelException(String mensagem) {
        super(mensagem);
    }
}
