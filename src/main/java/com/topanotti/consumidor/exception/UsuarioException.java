package com.topanotti.consumidor.exception;

public class UsuarioException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public UsuarioException(String mensagem) {
        super(mensagem);
    }
}
