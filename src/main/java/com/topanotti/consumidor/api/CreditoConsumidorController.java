package com.topanotti.consumidor.api;

import com.topanotti.consumidor.domain.model.CreditoConsumidor;
import com.topanotti.consumidor.service.CreditoConsumidorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/credito")
@Api(tags = "Api de crédito de consumidores")
public class CreditoConsumidorController {

    @Autowired
    CreditoConsumidorService creditoConsumidorService;


    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna o credito do consumidor conforme seu cpf"),
            @ApiResponse(code = 404, message = "Não existe o cpf informado"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @GetMapping(value="/{cpf}", produces = "application/json")
    public ResponseEntity<CreditoConsumidor> buscarPorCpf(@PathVariable String cpf) {
        CreditoConsumidor creditoConsumidor;
        try {
            creditoConsumidor = creditoConsumidorService.buscar(cpf);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(creditoConsumidor);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Retorna um crédito do consumidor recém salvo no banco"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @PostMapping(produces = "application/json")
    public ResponseEntity<?> salvar(@RequestBody CreditoConsumidor creditoConsumidor) {
        try {
            creditoConsumidor = creditoConsumidorService.salvar(creditoConsumidor);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(creditoConsumidor);
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                    .body(e.getMessage());
        }
    }


    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Crédito foi deletado com sucesso"),
            @ApiResponse(code = 404, message = "Não existe registro com o cpf informado"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @DeleteMapping(value = "/{cpf}")
    public ResponseEntity<?> remover(@PathVariable String cpf) {
        try {
            creditoConsumidorService.removerCredito(cpf);
            return ResponseEntity.noContent().build();

        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
