package com.topanotti.consumidor.api;

import com.topanotti.consumidor.domain.model.DividasConsumidor;
import com.topanotti.consumidor.service.DividasConsumidorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/divida")
@Api(tags = "Api de dívida de consumidores")
public class DividasConsumidorController {

    @Autowired
    DividasConsumidorService dividaConsumidorService;


    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna a dívida do consumidor conforme seu cpf"),
            @ApiResponse(code = 404, message = "Não existe o cpf informado"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @GetMapping(value="/{cpf}", produces = "application/json")
    public ResponseEntity<DividasConsumidor> buscarPorCpf(@PathVariable String cpf) {
        DividasConsumidor dividaConsumidor;
        try {
            dividaConsumidor = dividaConsumidorService.buscar(cpf);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(dividaConsumidor);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Retorna uma dívida do consumidor recém salvo no banco"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @PostMapping(produces = "application/json")
    public ResponseEntity<?> salvar(@RequestBody DividasConsumidor dividaConsumidor) {
        try {
            dividaConsumidor = dividaConsumidorService.salvar(dividaConsumidor);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(dividaConsumidor);
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                    .body(e.getMessage());
        }
    }


    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Dívida foi excluída com sucesso"),
            @ApiResponse(code = 404, message = "Não existe registro com o cpf informado"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @DeleteMapping(value = "/{cpf}")
    public ResponseEntity<?> remover(@PathVariable String cpf) {
        try {
            dividaConsumidorService.removerDividas(cpf);
            return ResponseEntity.noContent().build();

        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
