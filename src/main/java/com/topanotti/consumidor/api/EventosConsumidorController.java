package com.topanotti.consumidor.api;

import com.topanotti.consumidor.domain.model.EventosConsumidor;
import com.topanotti.consumidor.service.EventosConsumidorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/evento")
@Api(tags = "Api de eventos dos consumidores")
public class EventosConsumidorController {

    @Autowired
    EventosConsumidorService eventosConsumidorService;


    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna os eventos de um consumidor conforme seu cpf"),
            @ApiResponse(code = 404, message = "Não existe o cpf informado"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @GetMapping(value="/{cpf}", produces = "application/json")
    public ResponseEntity<EventosConsumidor> buscarPorCpf(@PathVariable String cpf) {
        EventosConsumidor eventoConsumidor;
        try {
            eventoConsumidor = eventosConsumidorService.buscar(cpf);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(eventoConsumidor);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Retorna um evento do consumidor recém salvo no banco"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @PostMapping(produces = "application/json")
    public ResponseEntity<?> salvar(@RequestBody EventosConsumidor eventoConsumidor) {
        try {
            eventoConsumidor = eventosConsumidorService.salvar(eventoConsumidor);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(eventoConsumidor);
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                    .body(e.getMessage());
        }
    }


    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Evento foi excluído com sucesso"),
            @ApiResponse(code = 404, message = "Não existe registro com o cpf informado"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @DeleteMapping(value = "/{cpf}")
    public ResponseEntity<?> remover(@PathVariable String cpf) {
        try {
            eventosConsumidorService.removerEventos(cpf);
            return ResponseEntity.noContent().build();

        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
