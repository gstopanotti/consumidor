package com.topanotti.consumidor.api;

import com.topanotti.consumidor.domain.model.Usuario;
import com.topanotti.consumidor.exception.UsuarioException;
import com.topanotti.consumidor.service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/usuarios")
@Api(tags = "Api de usuários")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;


    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna uma lista com os usuários"),
            @ApiResponse(code = 404, message = "Não existem usuários cadastrados"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @GetMapping(produces = "application/json")
    public ResponseEntity<?> buscarTodos() {
        List<Usuario> usuarios = usuarioService.buscarTodos();
        if (usuarios == null || usuarios.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(usuarios);
    }


    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna um usuário conforme seu login"),
            @ApiResponse(code = 404, message = "Não existe usuário com o login informado"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @GetMapping(value="/{loginUsuario}", produces = "application/json")
    public ResponseEntity<Usuario> buscarPorNome(@PathVariable String loginUsuario) {
        Usuario usuario;
        try {
            usuario = usuarioService.buscar(loginUsuario);
        } catch (UsuarioException e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(usuario);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Retorna um usuário recém salvo no banco"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @PostMapping(produces = "application/json")
    public ResponseEntity<?> salvar(@RequestBody Usuario usuario) {
        try {
            usuario = usuarioService.salvar(usuario);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(usuario);
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                    .body(e.getMessage());
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Atualiza as propriedades de um usuário"),
            @ApiResponse(code = 404, message = "Não existe usuário com o login informado"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @PutMapping(produces = "application/json")
    public ResponseEntity<?> atualizar(@RequestBody Usuario usuario) {
        try {
            Usuario usuarioAtualizado = usuarioService.atualizar(usuario);
            return ResponseEntity.ok(usuarioAtualizado);
        } catch (UsuarioException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Usuário foi deletado com sucesso"),
            @ApiResponse(code = 404, message = "Não existe usuário com o login informado"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @DeleteMapping(value = "/{loginUsuario}")
    public ResponseEntity<?> remover(@PathVariable String loginUsuario) {
        try {
            usuarioService.remover(loginUsuario);
            return ResponseEntity.noContent().build();

        } catch (UsuarioException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
