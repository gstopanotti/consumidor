package com.topanotti.consumidor.service;

import com.topanotti.consumidor.domain.model.Usuario;
import com.topanotti.consumidor.domain.repository.UsuarioRepository;
import com.topanotti.consumidor.dto.LoginUsuarioDto;
import com.topanotti.consumidor.exception.UsuarioException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private UsuarioRepository repository;

    public List<Usuario> buscarTodos() {
        return repository.listarTodos();
    }

    public Usuario buscar(String login) throws UsuarioException {
        return repository.buscar(login).orElseThrow(() -> new UsuarioException(String.format("Usuário %s não encontrado", login)));
    }

    public Usuario validaUsuarioSenha(LoginUsuarioDto loginUsuario) throws UsuarioException {
        return repository.validarUsuarioSenha(loginUsuario);
    }

    public Usuario atualizar(Usuario usuario) throws UsuarioException {
        Usuario usuarioAtual = buscar(usuario.getLogin());
        BeanUtils.copyProperties(usuario, usuarioAtual, "login", "senha");

        return repository.salvar(usuarioAtual);
    }

    public Usuario salvar(Usuario usuario) {
        repository.buscar(usuario.getLogin()).ifPresent(user -> {
            throw new UsuarioException(
                    String.format("Usuário %s já existe", user.getLogin()));
        });
        Usuario usuarioNovo = new Usuario();
        BeanUtils.copyProperties(usuario, usuarioNovo);
        usuarioNovo.setSenha(encoder.encode(usuario.getSenha()));
        return repository.salvar(usuarioNovo);
    }

    public void remover(String login) throws UsuarioException {
        repository.remover(login);
    }
}
