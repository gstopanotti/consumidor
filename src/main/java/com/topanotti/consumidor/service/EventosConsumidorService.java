package com.topanotti.consumidor.service;

import com.topanotti.consumidor.domain.model.EventosConsumidor;
import com.topanotti.consumidor.domain.model.MovimentacaoFinanceira;
import com.topanotti.consumidor.domain.repository.EventosConsumidorRepository;
import com.topanotti.consumidor.exception.ModelException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventosConsumidorService {

    @Autowired
    private EventosConsumidorRepository repository;


    public EventosConsumidor buscar(String cpf) throws ModelException {
        return repository.buscar(cpf).orElseThrow(() -> new ModelException(java.lang.String.format("CPF: %s não encontrado", cpf)));
    }

    public EventosConsumidor salvar(EventosConsumidor eventosConsumidor) {
        EventosConsumidor eventoNovo = new EventosConsumidor();

        BeanUtils.copyProperties(eventosConsumidor, eventoNovo);

        for(MovimentacaoFinanceira mov : eventosConsumidor.getMovimentacaoFinanceira()){
            mov.setCpf(eventoNovo);
        }
        if (eventoNovo.getUltimaCompraCartaoCredito() != null) {
            eventoNovo.getUltimaCompraCartaoCredito().setCpf(eventosConsumidor);
        }

        return repository.salvar(eventoNovo);
    }


    public void removerEventos(String cpf) throws ModelException {
        repository.remover(cpf);
    }
}
