package com.topanotti.consumidor.service;

import com.topanotti.consumidor.domain.model.Bens;
import com.topanotti.consumidor.domain.model.CreditoConsumidor;
import com.topanotti.consumidor.domain.repository.CreditoConsumidorRepository;
import com.topanotti.consumidor.exception.ModelException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreditoConsumidorService {

    @Autowired
    private CreditoConsumidorRepository repository;


    public CreditoConsumidor buscar(String cpf) throws ModelException {
        return repository.buscar(cpf).orElseThrow(() -> new ModelException(String.format("CPF %s não encontrado", cpf)));
    }

    public CreditoConsumidor salvar(CreditoConsumidor creditoConsumidor) {
        CreditoConsumidor eventoNovo = new CreditoConsumidor();

        BeanUtils.copyProperties(creditoConsumidor, eventoNovo);

        for(Bens bem : eventoNovo.getBens()){
            bem.setCpf(creditoConsumidor);
        }

        return repository.salvar(eventoNovo);
    }

    public void removerCredito(String cpf) throws ModelException {
        repository.remover(cpf);
    }
}
