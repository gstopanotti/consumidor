package com.topanotti.consumidor.service;

import com.topanotti.consumidor.domain.model.Divida;
import com.topanotti.consumidor.domain.model.DividasConsumidor;
import com.topanotti.consumidor.domain.repository.DividasConsumidorRepository;
import com.topanotti.consumidor.exception.ModelException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DividasConsumidorService {

    @Autowired
    private DividasConsumidorRepository repository;


    public DividasConsumidor buscar(String cpf) throws ModelException {
        return repository.buscar(cpf).orElseThrow(() -> new ModelException(String.format("Id %s não encontrado", cpf)));
    }

    public DividasConsumidor salvar(DividasConsumidor dividaConsumidor) {
        DividasConsumidor dividaNova = new DividasConsumidor();

        BeanUtils.copyProperties(dividaConsumidor, dividaNova);

        for(Divida divida : dividaNova.getDividas()){
            divida.setCpf(dividaConsumidor);
        }

        return repository.salvar(dividaNova);
    }

    public void removerDividas(String cpf) throws ModelException {
        repository.remover(cpf);
    }
}
