package com.topanotti.consumidor.domain.repository;

import com.topanotti.consumidor.domain.model.DividasConsumidor;
import com.topanotti.consumidor.exception.ModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Optional;

@Component
public class DividasConsumidorRepositoryImpl implements DividasConsumidorRepository {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    UsuarioRepository repository;

    @Override
    public Optional<DividasConsumidor> buscar(String cpf) {
        return Optional.ofNullable(em.find(DividasConsumidor.class, cpf));
    }
    @Transactional
    @Override
    public DividasConsumidor salvar(DividasConsumidor dividaConsumidor) {
        return em.merge(dividaConsumidor);
    }

    @Transactional
    @Override
    public void remover(String cpf) {
        Optional<DividasConsumidor> dividaConsumidor = buscar(cpf);

        if (dividaConsumidor.isPresent()) {
            em.remove(dividaConsumidor.get());
        } else {
            throw new ModelException("O regitro não existe.");
        }

    }
}
