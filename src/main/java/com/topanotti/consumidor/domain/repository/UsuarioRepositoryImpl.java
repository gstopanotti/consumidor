package com.topanotti.consumidor.domain.repository;

import com.topanotti.consumidor.domain.model.Usuario;
import com.topanotti.consumidor.dto.LoginUsuarioDto;
import com.topanotti.consumidor.exception.UsuarioException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class UsuarioRepositoryImpl implements UsuarioRepository {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    UsuarioRepository repository;

    @Override
    public List<Usuario> listarTodos() {
        return em.createQuery("from Usuario", Usuario.class)
                .getResultList();
    }

    @Override
    public Optional<Usuario> buscar(String login) {
        return Optional.ofNullable(em.find(Usuario.class, login));
    }

    @Transactional
    @Override
    public Usuario salvar(Usuario usuario) {
        return em.merge(usuario);
    }

    @Transactional
    @Override
    public void remover(String loginUsuario) {
        Optional<Usuario> usuario = buscar(loginUsuario);
        if (usuario.isPresent()) {
            em.remove(usuario.get());
        } else {
            throw new UsuarioException(String.format("O usuario %s não existe.", usuario));
        }
    }

    @Override
    public Usuario validarUsuarioSenha(LoginUsuarioDto loginUsuarioDto) {
        try {
            TypedQuery<Usuario> query = em.createQuery("select u from Usuario u where u.login = :login and u.senha = :senha", Usuario.class);
            query.setParameter("login", loginUsuarioDto.getLogin());
            query.setParameter("senha", loginUsuarioDto.getSenha());

            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
