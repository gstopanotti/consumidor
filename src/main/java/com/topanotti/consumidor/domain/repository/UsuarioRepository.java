package com.topanotti.consumidor.domain.repository;

import com.topanotti.consumidor.domain.model.Usuario;
import com.topanotti.consumidor.dto.LoginUsuarioDto;

import java.util.List;
import java.util.Optional;

public interface UsuarioRepository {

    List<Usuario> listarTodos();
    Optional<Usuario> buscar(String usuario);
    Usuario salvar(Usuario usuario);
    void remover(String usuario);
    Usuario validarUsuarioSenha(LoginUsuarioDto loginUsuarioDto);
}
