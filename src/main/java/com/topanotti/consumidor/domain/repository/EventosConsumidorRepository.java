package com.topanotti.consumidor.domain.repository;

import com.topanotti.consumidor.domain.model.EventosConsumidor;

import java.util.Optional;

public interface EventosConsumidorRepository {
    Optional<EventosConsumidor> buscar(String cpf);
    EventosConsumidor salvar(EventosConsumidor usuario);
    void remover(String cpf);
}
