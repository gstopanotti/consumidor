package com.topanotti.consumidor.domain.repository;

import com.topanotti.consumidor.domain.model.CreditoConsumidor;
import com.topanotti.consumidor.exception.ModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Optional;

@Component
public class CreditoConsumidorRepositoryImpl implements CreditoConsumidorRepository {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    UsuarioRepository repository;

    @Override
    public Optional<CreditoConsumidor> buscar(String cpf) {
        return Optional.ofNullable(em.find(CreditoConsumidor.class, cpf));
    }
    @Transactional
    @Override
    public CreditoConsumidor salvar(CreditoConsumidor creditoConsumidor) {
        return em.merge(creditoConsumidor);
    }


    @Transactional
    @Override
    public void remover(String cpf) {
        Optional<CreditoConsumidor> creditoConsumidor = buscar(cpf);

        if (creditoConsumidor.isPresent()) {
            em.remove(creditoConsumidor.get());
        } else {
            throw new ModelException("O regitro não existe.");
        }

    }
}
