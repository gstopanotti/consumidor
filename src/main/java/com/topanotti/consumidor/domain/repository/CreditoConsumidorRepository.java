package com.topanotti.consumidor.domain.repository;

import com.topanotti.consumidor.domain.model.CreditoConsumidor;

import java.util.Optional;

public interface CreditoConsumidorRepository {
    Optional<CreditoConsumidor> buscar(String cpf);
    CreditoConsumidor salvar(CreditoConsumidor credito);
    void remover(String cpf);
}
