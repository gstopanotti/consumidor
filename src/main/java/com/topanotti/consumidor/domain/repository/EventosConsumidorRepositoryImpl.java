package com.topanotti.consumidor.domain.repository;

import com.topanotti.consumidor.domain.model.EventosConsumidor;
import com.topanotti.consumidor.exception.ModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Optional;

@Component
public class EventosConsumidorRepositoryImpl implements EventosConsumidorRepository {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    UsuarioRepository repository;

    @Override
    public Optional<EventosConsumidor> buscar(String cpf) {
        return Optional.ofNullable(em.find(EventosConsumidor.class, cpf));
    }
    @Transactional
    @Override
    public EventosConsumidor salvar(EventosConsumidor eventosConsumidor) {
        return em.merge(eventosConsumidor);
    }

    @Transactional
    @Override
    public void remover(String cpf ) {
        Optional<EventosConsumidor> eventosConsumidor = buscar(cpf);

        if (eventosConsumidor.isPresent()) {
            em.remove(eventosConsumidor.get());
        } else {
            throw new ModelException("O regitro não existe.");
        }

    }
}
