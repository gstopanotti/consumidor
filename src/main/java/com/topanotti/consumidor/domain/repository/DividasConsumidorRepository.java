package com.topanotti.consumidor.domain.repository;

import com.topanotti.consumidor.domain.model.DividasConsumidor;

import java.util.Optional;

public interface DividasConsumidorRepository {
    Optional<DividasConsumidor> buscar(String cpf);
    DividasConsumidor salvar(DividasConsumidor divida);
    void remover(String cpf);
}
