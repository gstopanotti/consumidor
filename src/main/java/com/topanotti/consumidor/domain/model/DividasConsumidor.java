package com.topanotti.consumidor.domain.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class DividasConsumidor {

    @ApiModelProperty(value = "CPF do consumidor")
    @Id
    @NotNull
    private String cpf;

    @ApiModelProperty(value = "Nome do consumidor")
    @NotNull
    private String nome;

    @ApiModelProperty(value = "Endereço do consumidor")
    private String endereco;

    @JsonManagedReference
    @ApiModelProperty(value = "Divida do consumidor")
    @OneToMany(
            mappedBy = "cpf",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    private List<Divida> dividas;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public List<Divida> getDividas() {
        return dividas;
    }

    public void setDividas(List<Divida> dividas) {
        this.dividas = dividas;
    }
}
