package com.topanotti.consumidor.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Bens {

    @ApiModelProperty(value = "Id no banco de dados")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    @ApiModelProperty(value = "CPF do Consumidor")
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="cpf")
    @NotNull
    private CreditoConsumidor cpf;

    @ApiModelProperty(value = "Descrição do bem")
    @NotNull
    private String descricao;


    @ApiModelProperty(value = "Valor do bem")
    @NotNull
    private Long valor;


    public Bens() {
    }

    public Bens(Long id, @NotNull CreditoConsumidor cpf, @NotNull String descricao, @NotNull Long valor) {
        this.id = id;
        this.cpf = cpf;
        this.descricao = descricao;
        this.valor = valor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CreditoConsumidor getCpf() {
        return cpf;
    }

    public void setCpf(CreditoConsumidor cpf) {
        this.cpf = cpf;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }
}
