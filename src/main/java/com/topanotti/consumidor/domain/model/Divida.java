package com.topanotti.consumidor.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Divida {

    @ApiModelProperty(value = "Id no banco de dados")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    @ApiModelProperty(value = "CPF do consumidor")
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="cpf")
    @NotNull
    private DividasConsumidor cpf;

    @ApiModelProperty(value = "Descrição da dívida do consumidor")
    @NotNull
    private String descricao;

    @ApiModelProperty(value = "Valor da dívida do consumidor")
    @NotNull
    private Long valor;

    public DividasConsumidor getCpf() {
        return cpf;
    }

    public void setCpf(DividasConsumidor cpf) {
        this.cpf = cpf;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }
}
