
package com.topanotti.consumidor.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
public class UltimaCompraCartaoCredito {

    @ApiModelProperty(value = "Id no banco de dados")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    @ApiModelProperty(value = "CPF do consumidor")
    @JsonBackReference
    @OneToOne
    @JoinColumn(name="cpf")
    @NotNull
    private EventosConsumidor cpf;

    @ApiModelProperty(value = "Descricao da compra")
    @NotNull
    private String descricao;

    @ApiModelProperty(value = "Valor da compra")
    @NotNull
    private Long valor;

    @ApiModelProperty(value = "Data e hora da compra")
    @JsonFormat(pattern="dd-MM-yy HH:mm:ss")
    @NotNull
    private LocalDateTime dataHora;


    public EventosConsumidor getCpf() {
        return cpf;
    }

    public void setCpf(EventosConsumidor cpf) {
        this.cpf = cpf;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }

    public LocalDateTime getDataHora() {
        return dataHora;
    }

    public void setDataHora(LocalDateTime dataHora) {
        this.dataHora = dataHora;
    }
}

