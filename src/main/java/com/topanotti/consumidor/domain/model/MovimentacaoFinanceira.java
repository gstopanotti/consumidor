package com.topanotti.consumidor.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class MovimentacaoFinanceira {

    @ApiModelProperty(value = "Id no banco de dados")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    @ApiModelProperty(value = "CPF do consumidor")
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="cpf")
    @NotNull
    private EventosConsumidor cpf;


    @ApiModelProperty(value = "Descrição da movimentação")
    @NotNull
    private String descricao;

    @ApiModelProperty(value = "Valor da movimentação")
    @NotNull
    private Long valor;

    public EventosConsumidor getCpf() {
        return cpf;
    }

    public void setCpf(EventosConsumidor cpf) {
        this.cpf = cpf;
    }

    public java.lang.String getDescricao() {
        return descricao;
    }

    public void setDescricao(java.lang.String descricao) {
        this.descricao = descricao;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }
}
