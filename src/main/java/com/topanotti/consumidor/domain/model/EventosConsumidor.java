package com.topanotti.consumidor.domain.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class EventosConsumidor {

    @ApiModelProperty(value = "CPF do consumidor")
    @Id
    @NotNull
    private java.lang.String cpf;

    @JsonManagedReference
    @ApiModelProperty(value = "Movimentações do consumidor")
    @OneToMany(
            mappedBy = "cpf",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<MovimentacaoFinanceira> movimentacaoFinanceira;

    @JsonManagedReference
    @ApiModelProperty(value = "Última compra no cartão de crédito do consumidor")
    @OneToOne(
            mappedBy = "cpf",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    private UltimaCompraCartaoCredito ultimaCompraCartaoCredito;

    public java.lang.String getCpf() {
        return cpf;
    }

    public void setCpf(java.lang.String cpf) {
        this.cpf = cpf;
    }

    public List<MovimentacaoFinanceira> getMovimentacaoFinanceira() {
        return movimentacaoFinanceira;
    }

    public void setMovimentacaoFinanceira(List<MovimentacaoFinanceira> movimentacaoFinanceira) {
        this.movimentacaoFinanceira = movimentacaoFinanceira;
    }

    public UltimaCompraCartaoCredito getUltimaCompraCartaoCredito() {
        return ultimaCompraCartaoCredito;
    }

    public void setUltimaCompraCartaoCredito(UltimaCompraCartaoCredito ultimaCompraCartaoCredito) {
        this.ultimaCompraCartaoCredito = ultimaCompraCartaoCredito;
    }
}
