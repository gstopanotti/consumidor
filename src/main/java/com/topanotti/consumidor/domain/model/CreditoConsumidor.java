package com.topanotti.consumidor.domain.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class CreditoConsumidor {

    @ApiModelProperty(value = "CPF do consumidor")
    @Id
    @NotNull
    private String cpf;

    @ApiModelProperty(value = "Nome do consumidor")
    @NotNull
    private String nome;

    @ApiModelProperty(value = "Idade do consumidor")
    @NotNull
    private Integer idade;

    @JsonManagedReference
    @ApiModelProperty(value = "bens do consumidor")
    @OneToMany(
            mappedBy = "cpf",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    private List<Bens> bens;

    @ApiModelProperty(value = "Endereço do consumidor")
    private String endereco;

    @ApiModelProperty(value = "Fonte de renda")
    private String fonteDeRenda;

    public CreditoConsumidor() {
    }

    public CreditoConsumidor(@NotNull String cpf, @NotNull String nome, @NotNull Integer idade, List<com.topanotti.consumidor.domain.model.Bens> bens, String endereco, String fonteDeRenda) {
        this.cpf = cpf;
        this.nome = nome;
        this.idade = idade;
        this.bens = bens;
        this.endereco = endereco;
        this.fonteDeRenda = fonteDeRenda;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public List<com.topanotti.consumidor.domain.model.Bens> getBens() {
        return bens;
    }

    public void setBens(List<com.topanotti.consumidor.domain.model.Bens> bens) {
        this.bens = bens;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getFonteDeRenda() {
        return fonteDeRenda;
    }

    public void setFonteDeRenda(String fonteDeRenda) {
        this.fonteDeRenda = fonteDeRenda;
    }
}
