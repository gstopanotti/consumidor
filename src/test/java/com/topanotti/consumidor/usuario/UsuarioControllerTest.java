package com.topanotti.consumidor.usuario;

import com.topanotti.consumidor.ConsumidorApplicationTests;
import com.topanotti.consumidor.api.UsuarioController;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UsuarioControllerTest extends ConsumidorApplicationTests {

    private MockMvc mock;

    @Autowired
    private UsuarioController usuarioController;

    private String jsonValue;


    @Before
    public void setUp() {
        this.mock = MockMvcBuilders.standaloneSetup(usuarioController).build();
    }

    @Before
    public void montaJsonParaTeste() throws Exception {
        JSONObject json = new JSONObject();
        try {
            json.put("login", "guilherme");
            json.put("senha", "1234");
            json.put("nome", "Usuario de Teste");
        } catch (JSONException e) {
            throw new Exception("ERRO AO MONTAR O JSON PARA TESTE");
        }

        this.jsonValue = json.toString();
    }

    @Test
    public void deveAtualizarUmUsuario() throws Exception {
        this.mock.perform(
                MockMvcRequestBuilders.put("/usuarios")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonValue))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void deveBuscarListaDeUsuarios() throws Exception {
        this.mock.perform(MockMvcRequestBuilders.get("/usuarios")).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deveBuscarUsuarioPeloLogin() throws Exception {
        this.mock.perform(MockMvcRequestBuilders.get("/usuarios/guilherme")).andExpect(MockMvcResultMatchers.status().isOk());
    }


    @Test
    public void deveRemoverUmUsuario() throws Exception {
        MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonValue);

        this.mock.perform(
                MockMvcRequestBuilders.delete("/usuarios/guilherme"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void deveSalvarUmUsuarioNovo() throws Exception {
        this.mock.perform(
                MockMvcRequestBuilders.post("/usuarios")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonValue))
                .andExpect(MockMvcResultMatchers.status().isCreated());

    }

}
