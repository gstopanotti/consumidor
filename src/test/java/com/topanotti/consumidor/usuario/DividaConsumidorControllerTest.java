package com.topanotti.consumidor.usuario;

import com.topanotti.consumidor.ConsumidorApplicationTests;
import com.topanotti.consumidor.api.DividasConsumidorController;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DividaConsumidorControllerTest extends ConsumidorApplicationTests {

    private MockMvc mock;

    @Autowired
    private DividasConsumidorController controller;

    private String jsonValue;


    @Before
    public void setUp() {
        this.mock = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Before
    public void montaJsonParaTeste() throws Exception {
        JSONObject json = new JSONObject();
        JSONObject jsonDivida = new JSONObject();
        try {
            jsonDivida.put("descricao", "Conta na loja X");
            jsonDivida.put("valor", "350");


            json.put("cpf", "123");
            json.put("nome", "Usuario de Teste");
            json.put("endereco", "RUA DE CASA");
            json.put("dividas", new JSONArray().put(jsonDivida));

        } catch (JSONException e) {
            throw new Exception("ERRO AO MONTAR O JSON PARA TESTE");
        }

        this.jsonValue = json.toString();
    }

    @Test
    public void deve1SalvarUmaDividaNova() throws Exception {
        this.mock.perform(
                MockMvcRequestBuilders.post("/divida")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonValue))
                .andExpect(MockMvcResultMatchers.status().isCreated());

    }

    @Test
    public void deve2BuscarDividaPeloCpf() throws Exception {
        this.mock.perform(MockMvcRequestBuilders.get("/divida/123")).andExpect(MockMvcResultMatchers.status().isOk());
    }


    @Test
    public void deve3RemoverUmaDivida() throws Exception {
        MockMvcRequestBuilders.post("/divida")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonValue);

        this.mock.perform(
                MockMvcRequestBuilders.delete("/divida/123"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

}
