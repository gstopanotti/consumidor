package com.topanotti.consumidor.usuario;

import com.topanotti.consumidor.ConsumidorApplicationTests;
import com.topanotti.consumidor.api.CreditoConsumidorController;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CreditoConsumidorControllerTest extends ConsumidorApplicationTests {

    private MockMvc mock;

    @Autowired
    private CreditoConsumidorController controller;

    private String jsonValue;


    @Before
    public void setUp() {
        this.mock = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Before
    public void montaJsonParaTeste() throws Exception {
        JSONObject json = new JSONObject();
        JSONObject jsonBens = new JSONObject();
        try {
            jsonBens.put("descricao", "Carro popular");
            jsonBens.put("valor", "30000");


            json.put("cpf", "123");
            json.put("nome", "Usuario de Teste");
            json.put("idade", "30");
            json.put("endereco", "RUA DE CASA");
            json.put("fonteDeRenda", "trabalho");
            json.put("bens", new JSONArray().put(jsonBens));

        } catch (JSONException e) {
            throw new Exception("ERRO AO MONTAR O JSON PARA TESTE");
        }

        this.jsonValue = json.toString();
    }

    @Test
    public void deve1SalvarUmCreditoNovo() throws Exception {
        this.mock.perform(
                MockMvcRequestBuilders.post("/credito")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonValue))
                .andExpect(MockMvcResultMatchers.status().isCreated());

    }

    @Test
    public void deve2BuscarCreditoPeloCpf() throws Exception {
        //deve1SalvarUmCreditoNovo();
        this.mock.perform(MockMvcRequestBuilders.get("/credito/123")).andExpect(MockMvcResultMatchers.status().isOk());
    }


    @Test
    public void deve3RemoverUmCredito() throws Exception {
        MockMvcRequestBuilders.post("/credito")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonValue);

        this.mock.perform(
                MockMvcRequestBuilders.delete("/credito/123"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

}
